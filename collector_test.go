package main

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"strings"
	"testing"
	"time"

	goJSON "github.com/goccy/go-json"
)

var (
	//nolint:lll
	testStatus1 = `
{
  "Astrogator": {
    "frozen": false,
    "last_checked": "2021-01-07T03:08:10+00:00",
    "last_downloaded": "2021-03-18T02:39:40.181818+00:00",
    "last_error": null,
    "last_indexed": "2021-01-08T23:08:41.855922+00:00",
    "last_inflated": "2021-03-21T10:08:10+00:00",
    "last_warnings": null,
    "release_date": "2020-10-28T05:31:47+00:00",
    "resources": {
      "remote-avc": "https://raw.githubusercontent.com/HebaruSan/Astrogator/master/Astrogator.version",
      "bugtracker": "https://github.com/HebaruSan/Astrogator/issues",
      "repository": "https://github.com/HebaruSan/Astrogator",
      "homepage": "https://forum.kerbalspaceprogram.com/index.php?/topic/155998-*"
    },
    "success": true
  },
  "PlanningNode": {
    "frozen": false,
    "last_checked": null,
    "last_downloaded": "2021-03-13T05:44:10.952283+00:00",
    "last_error": null,
    "last_indexed": "2021-03-13T05:44:15.906934+00:00",
    "last_inflated": "2021-03-21T10:15:15+00:00",
    "last_warnings": null,
    "release_date": "2021-03-13T05:31:36+00:00",
    "resources": {
      "remote-avc": "https://raw.githubusercontent.com/HebaruSan/PlanningNode/master/GameData/PlanningNode/PlanningNode.version",
      "bugtracker": "https://github.com/HebaruSan/PlanningNode/issues",
      "repository": "https://github.com/HebaruSan/PlanningNode",
      "homepage": "https://forum.kerbalspaceprogram.com/index.php?/topic/200375-*"
    },
    "success": true
  }
}`
)

func falsePointer() *bool {
	a := false
	return &a
}

func Test_fetchStatus(t *testing.T) {
	type test struct {
		name    string
		handler http.Handler
		wantErr bool
	}

	tests := []test{
		{
			"Fetch testStatus1",
			http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) { _, _ = fmt.Fprintln(w, testStatus1) }),
			false,
		},
		{
			"Fetch local file",
			http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { http.ServeFile(w, r, "./testdata/status.json") }),
			false,
		},
	}

	cache := &statusCache{}

	testServer := httptest.NewServer(nil)
	defer testServer.Close()

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testServer.Config.Handler = tt.handler

			data, _, err := fetchStatus(testServer.URL, cache)
			if (err != nil) != tt.wantErr {
				t.Errorf("fetchStatus() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if (data == nil) != tt.wantErr {
				t.Errorf("fetchStatus() data = %v, wantErr %v", data, tt.wantErr)
				return
			}
		})
	}
}

func Test_fetchStatus_cache(t *testing.T) {
	testServer := httptest.NewServer(nil)
	defer testServer.Close()
	testServer.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./testdata/status.json")
	})

	cache := statusCache{
		maxAge: time.Duration(math.MaxInt64),
	}

	data1, stats1, err1 := fetchStatus(testServer.URL, &cache)
	if err1 != nil {
		t.Errorf("fetchStatus() error1 = %v, wantErr %v", err1, nil)
		return
	}
	if data1 != cache.lastStatus {
		t.Errorf("fetchStatus() data1 = %p, want %p (does not match cache)", data1, cache.lastStatus)
		return
	}
	if stats1.downloadDuration == time.Duration(0) || stats1.parsingDuration == time.Duration(0) {
		t.Errorf("fetchStatus() downloadDuration = %v, parsingDuration = %v, want %v",
			stats1.downloadDuration, stats1.parsingDuration, ">0")
		return
	}

	data2, stats2, err2 := fetchStatus(testServer.URL, &cache)
	if err2 != nil {
		t.Errorf("fetchStatus() error = %v, wantErr %v", err2, nil)
		return
	}
	if data2 != cache.lastStatus {
		t.Errorf("fetchStatus() data2 = %p, want %p (does not match cache)", data2, cache.lastStatus)
		return
	}
	if data2 != data1 {
		t.Errorf("fetchStatus() data2 = %p, want %p (does not match first result)", data2, data1)
		return
	}
	if stats2.downloadDuration != time.Duration(0) || stats2.parsingDuration != time.Duration(0) {
		t.Errorf("fetchStatus() downloadDuration = %v, parsingDuration = %v, want %v",
			stats2.downloadDuration, stats2.parsingDuration, 0)
		return
	}
}

func getStatusObject(str string) *Status {
	statusData := new(Status)
	_ = json.Unmarshal([]byte(str), statusData)
	return statusData
}

func getStatusObjectFromCompressedFile(gzFile string, useGoJSON bool) (*Status, error) {
	statusData := new(Status)
	file, err := os.Open(gzFile)
	if err != nil {
		return nil, err
	}
	gzReader, err := gzip.NewReader(file)
	if err != nil {
		return nil, err
	}
	if useGoJSON {
		err = goJSON.NewDecoder(gzReader).Decode(statusData)
	} else {
		err = json.NewDecoder(gzReader).Decode(statusData)
	}
	if err != nil {
		return nil, err
	}
	return statusData, nil
}

func Test_parseStatus(t *testing.T) {
	type args struct {
		reader    io.Reader
		useGoJSON bool
	}

	tests := []struct {
		name           string
		args           args
		wantStatusData *Status
		wantErr        bool
	}{
		{
			"Simple Status JSON",
			args{strings.NewReader(testStatus1), false},
			getStatusObject(testStatus1),
			false,
		},
		{
			"Broken Status JSON",
			args{strings.NewReader("{ [ \"identifier\": {} ] }"), false},
			nil,
			true,
		},
		{
			"Simple Status JSON",
			args{strings.NewReader(testStatus1), true},
			getStatusObject(testStatus1),
			false,
		},
		{
			"Broken Status JSON",
			args{strings.NewReader("{ [ \"identifier\": {} ] }"), true},
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotStatusData, err := parseStatus(tt.args.reader, tt.args.useGoJSON)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseStatus() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotStatusData, tt.wantStatusData) {
				t.Errorf("parseStatus() gotStatusData = %v, want %v", gotStatusData, tt.wantStatusData)
			}
		})
	}
}

func TestStatusExporter_processMods(t *testing.T) {
	testData, _ := getStatusObjectFromCompressedFile("testdata/2021-05-16-status.json.gz", false)

	type args struct {
		concurrent     bool
		chanBufferSize int
		statusData     *Status
	}
	type want struct {
		activeMods int
	}
	tests := []struct {
		name string
		args args
		want want
	}{
		{
			"testdata_concurrent",
			args{true, 1024, testData},
			want{activeMods: 1281},
		},
		{
			"testdata_synchronous",
			args{false, 1024, testData},
			want{activeMods: 1281},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := NewStatusExporter("", time.Duration(0))
			e.processMods(tt.args.concurrent, tt.args.chanBufferSize, tt.args.statusData)

			if got := int(e.counters.activeModsCount.c); got != tt.want.activeMods {
				t.Errorf("expected %v got %v", tt.want.activeMods, got)
			}
			resetMetricCounters(&e.counters)
		})
	}
}

func benchmarkCollect(b *testing.B, concurrent bool) {
	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./testdata/status.json")
	}))
	defer testServer.Close()
	oldNoCache := noCache
	noCache = falsePointer()
	defer func() { noCache = oldNoCache }()

	exporter := NewStatusExporter(testServer.URL, time.Duration(0))

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		exporter.collect(concurrent)
	}
}

func BenchmarkCollectConcurrent(b *testing.B)  { benchmarkCollect(b, true) }
func BenchmarkCollectSynchronous(b *testing.B) { benchmarkCollect(b, false) }

// The next benchmark checks performance with different mod channel buffer sizes.
func BenchmarkProcessMods_BufferSize(b *testing.B) {
	oldNoCache := noCache
	noCache = falsePointer()
	defer func() { noCache = oldNoCache }()

	exporter := NewStatusExporter("", time.Duration(0))
	statusData, _ := getStatusObjectFromCompressedFile("testdata/2021-05-16-status.json.gz", false)

	b.ResetTimer()

	for _, i := range []int{0, 4, 64, 256, 512, 1024, 2048, 4096} {
		i := i
		b.Run(
			fmt.Sprintf("buffer_%d", i),
			func(b *testing.B) {
				for i := 0; i < b.N; i++ {
					exporter.processMods(true, i, statusData)
				}
			},
		)
	}
}

func benchmarkParseStatus(b *testing.B, useGoJSON bool) {
	file, err := os.Open("./testdata/status.json")
	if err != nil {
		panic(err)
	}
	jsonBytes, err := io.ReadAll(file)
	file.Close()
	if err != nil {
		panic(err)
	}

	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		reader := bytes.NewReader(jsonBytes)
		_, err = parseStatus(reader, useGoJSON)
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkParseStatusGoJSON(b *testing.B)  { benchmarkParseStatus(b, true) }
func BenchmarkParseStatusBuiltin(b *testing.B) { benchmarkParseStatus(b, false) }
