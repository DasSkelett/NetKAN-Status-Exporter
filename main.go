package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/alecthomas/kingpin/v2"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/version"
)

var (
	listenAddress = kingpin.Flag("web.listen-address",
		"Address to listen on for web interface and telemetry.").
		Default(":9811").
		Envar("NETKAN_STATUS_EXPORTER_WEB_LISTEN_ADDRESS").
		String()
	metricPath = kingpin.Flag("web.telemetry-path",
		"Path under which to expose metrics.").
		Default("/metrics").
		Envar("NETKAN_STATUS_EXPORTER_WEB_TELEMETRY_PATH").
		String()
	statusJSONSource = kingpin.Flag("netkan.status-source",
		"Address to fetch the status JSON from").
		Default("http://status.ksp-ckan.space/status/netkan.json").
		Envar("NETKAN_STATUS_EXPORTER_SOURCE").
		String()
	statusJSONMinRefresh = kingpin.Flag("netkan.min-refresh",
		"Minimum time between re-fetching status JSON, set to 0 to disable caching").
		Default("5m").
		Envar("NETKAN_STATUS_EXPORTER_MIN_REFRESH").
		Duration()
	useGoJSON = kingpin.Flag("netkan.use-gojson",
		"[EXPERIMENTAL] Use goccy/go-json instead of encoding/json").
		Default("false").
		Envar("NETKAN_STATUS_EXPORTER_GOJSON").
		Bool()
	noCache = kingpin.Flag("netkan.no-cache",
		"Don't cache status data between scrapes").
		Default("false").
		Envar("NETKAN_STATUS_EXPORTER_NO_CACHE").
		Bool()
	debug = kingpin.Flag("debug", "Turn on debug logging").
		Default("false").
		Envar("NETKAN_STATUS_EXPORTER_DEBUG").
		Bool()
)

const (
	exporterName = "NetKAN Status Exporter"
)

func main() {
	kingpin.Version(version.Print(exporterName))
	kingpin.HelpFlag.Short('h')
	kingpin.Parse()

	var webRootHTML = []byte(fmt.Sprintf(
		`<html>
		<head><title>%s</title></head>
		<body>
		<h1>NetKAN Status Exporter</h1>
		<p><a href='%s'>Metrics</a></p>
		</body>
		</html>
		`,
		exporterName, *metricPath,
	))

	var cacheMaxAge time.Duration
	if *noCache {
		cacheMaxAge = 0
	} else {
		cacheMaxAge = *statusJSONMinRefresh
	}

	exporter := NewStatusExporter(*statusJSONSource, cacheMaxAge)
	prometheus.MustRegister(exporter)

	http.Handle(*metricPath, promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "text/html; charset=UTF-8")
		_, _ = w.Write(webRootHTML)
	})

	// Prepare graceful shutdown
	signalChan := make(chan os.Signal, 2)
	signal.Notify(signalChan, os.Interrupt, syscall.SIGTERM)
	errChan := make(chan error)

	srv := http.Server{
		Addr: *listenAddress,
	}

	go func() {
		fmt.Println("Running NetKAN Status Exporter, listening on " + *listenAddress)
		err := srv.ListenAndServe()
		if err != nil && errors.Is(err, http.ErrServerClosed) {
			errChan <- fmt.Errorf("unable to start http server: %w", err)
		}
	}()

	select {
	case <-signalChan:
		fmt.Println("Shutting down...")
		ctx := context.Background()
		ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
		err := srv.Shutdown(ctx)
		if err != nil {
			log.Println(err)
		}
		cancel()
	case err := <-errChan:
		log.Println(err)
	}
}
