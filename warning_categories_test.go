package main

import (
	"runtime"
	"sync"
	"testing"
)

func Test_mmDepNoMmSyntax_regex(t *testing.T) {
	type fields struct {
		message string
	}
	//nolint:lll
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			"Valid mmDepNoMmSyntax warning",
			fields{"ModuleManager dependency may not be needed, no ModuleManager syntax found"},
			true,
		},
		{
			"Invalid mmDepNoMmSyntax warning",
			fields{"ModuleManager syntax used without ModuleManager dependency: CapsuleCorp/Parts/KKP_PatchAnim_Resources/Patch_1.0.cfg"},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mm := mmDepNoMmSyntax{}
			if match := mm.regex().MatchString(tt.fields.message); match != tt.want {
				t.Errorf("regex().MatchString() = %v, want %v", match, tt.want)
			}
		})
	}
}

func Test_mmSyntaxNoMmDep_regex(t *testing.T) {
	type fields struct {
		message string
	}
	//nolint:lll
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			"Valid mmSyntaxNoMmDep warning",
			fields{"ModuleManager syntax used without ModuleManager dependency: CapsuleCorp/Parts/KKP_PatchAnim_Resources/Patch_1.0.cfg"},
			true,
		},
		{
			"Invalid mmSyntaxNoMmDep warning",
			fields{"ModuleManager dependency may not be needed, no ModuleManager syntax found"},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mm := mmSyntaxNoMmDep{}
			if match := mm.regex().MatchString(tt.fields.message); match != tt.want {
				t.Errorf("regex().MatchString() = %v, want %v", match, tt.want)
			}
		})
	}
}

func Test_remoteVersionFileFetchError_regex(t *testing.T) {
	type fields struct {
		message string
	}
	//nolint:lll
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			"Valid remoteVersionFileFetchError warning",
			fields{"Error fetching remote version file http://ksp.spacetux.net/avc/Material-Science-Pod: The remote server returned an error: (404) Not Found."},
			true,
		},
		{
			"Valid remoteVersionFileFetchError multiline warning first",
			fields{"Error fetching remote version file https://github.com/GarwelGarwel/Timekeeper/raw/master/Timekeeper.version: The remote server returned an error: (404) Not Found.\r\nUnbounded future compatibility for module with a plugin, consider setting $vref or ksp_version or ksp_version_max"},
			true,
		},
		{
			"Valid remoteVersionFileFetchError multiline warning second",
			fields{"Remote non-raw GitHub URL is in an unknown format, using as is.\r\nError fetching remote version file https://github.com/zer0Kerbal/DockingPortDescriptions/DockingPortDescriptions.version: The remote server returned an error: (404) Not Found."},
			true,
		},
		{
			"Invalid remoteVersionFileFetchError warning",
			fields{"ModuleManager dependency may not be needed, no ModuleManager syntax found"},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mm := remoteVersionFileFetchError{}
			if match := mm.regex().MatchString(tt.fields.message); match != tt.want {
				t.Errorf("regex().MatchString() = %v, want %v", match, tt.want)
			}
		})
	}
}

func Test_unboundedCompatibility_regex(t *testing.T) {
	type fields struct {
		message string
	}
	//nolint:lll
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			"Valid unboundedCompatibility warning",
			fields{"Unbounded future compatibility for module with a plugin, consider setting $vref or ksp_version or ksp_version_max"},
			true,
		},
		{
			"Valid unboundedCompatibility multiline warning second",
			fields{"Error fetching remote version file https://github.com/GarwelGarwel/Timekeeper/raw/master/Timekeeper.version: The remote server returned an error: (404) Not Found.\r\nUnbounded future compatibility for module with a plugin, consider setting $vref or ksp_version or ksp_version_max"},
			true,
		},
		{
			"Invalid unboundedCompatibility warning",
			fields{"ModuleManager dependency may not be needed, no ModuleManager syntax found"},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mm := unboundedCompatibility{}
			if match := mm.regex().MatchString(tt.fields.message); match != tt.want {
				t.Errorf("regex().MatchString() = %v, want %v", match, tt.want)
			}
		})
	}
}

func Test_versionFileNoVref_regex(t *testing.T) {
	type fields struct {
		message string
	}
	//nolint:lll
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			"Valid versionFileNoVref warning",
			fields{"$vref absent, version file present: LMPClient/GameData/LunaMultiplayer/LunaMultiplayer.version"},
			true,
		},
		{
			"Invalid versionFileNoVref warning",
			fields{"$vref present, version file missing"},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mm := versionFileNoVref{}
			if match := mm.regex().MatchString(tt.fields.message); match != tt.want {
				t.Errorf("regex().MatchString() = %v, want %v", match, tt.want)
			}
		})
	}
}

func Test_vrefNoVersionFile_regex(t *testing.T) {
	type fields struct {
		message string
	}
	//nolint:lll
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			"Valid vrefNoVersionFile warning",
			fields{"$vref present, version file missing"},
			true,
		},
		{
			"Valid vrefNoVersionFile multiline warning",
			fields{"$vref present, version file missing\\r\\nUnbounded future compatibility for module with a plugin, consider setting $vref or ksp_version or ksp_version_max"},
			true,
		},
		{
			"Invalid vrefNoVersionFile warning",
			fields{"$vref absent, version file present: LMPClient/GameData/LunaMultiplayer/LunaMultiplayer.version"},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mm := vrefNoVersionFile{}
			if match := mm.regex().MatchString(tt.fields.message); match != tt.want {
				t.Errorf("regex().MatchString() = %v, want %v", match, tt.want)
			}
		})
	}
}

func testWarningCategoriesCorrectCounts(t *testing.T, useGoJSON bool) {
	inputData := make(chan Module, 100)
	wg := sync.WaitGroup{}

	warningCategories := []WarningCategory{
		vrefNoVersionFile{},
		versionFileNoVref{},
		remoteVersionFileFetchError{},
		remoteVersionFileParseError{},
		mmDepNoMmSyntax{},
		mmSyntaxNoMmDep{},
		unboundedCompatibility{},
		githubURLUnknownFormat{},
		dllsNotAutodetected{},
	}
	warningCategoriesCounts := make([]AtomicCounter, len(warningCategories))
	metrics := &modMetricsCounters{
		warningCategories:       &warningCategories,
		warningCategoriesCounts: &warningCategoriesCounts,
	}
	// cat 2021-05-16-status.json | \
	// jq '.[] | select(.frozen == false and .last_warnings != null) | \
	//	select(.last_warnings | contains("<PartOfRegex>")) | .last_warnings' | \
	// wc -l
	expectedWarningCategoriesCounts := []int{3, 3, 45, 7, 4, 26, 80, 5, 241}

	for i := 1; i <= runtime.NumCPU(); i++ {
		wg.Add(1)
		go collectWorker(metrics, inputData, &wg)
	}

	statusObject, err := getStatusObjectFromCompressedFile("testdata/2021-05-16-status.json.gz", useGoJSON)
	if err != nil {
		t.Fatal(err)
	}
	for _, mod := range *statusObject {
		inputData <- mod
	}
	close(inputData)
	wg.Wait()

	for i, cat := range *metrics.warningCategories {
		expected := expectedWarningCategoriesCounts[i]
		if expected == -1 {
			continue
		}
		actual := int((*metrics.warningCategoriesCounts)[i].c)
		if actual != expected {
			t.Errorf("Warning category %s: expected %d hits, got %d", cat.label(), expected, actual)
		}
	}
}

func TestWarningCategoriesCorrectCountsBuiltIn(t *testing.T) {
	testWarningCategoriesCorrectCounts(t, false)
}
func TestWarningCategoriesCorrectCountsGoJSON(t *testing.T) {
	testWarningCategoriesCorrectCounts(t, true)
}
