#!/bin/bash

set -ex

CI_USE_CACHE=${CI_USE_CACHE:-true}

params=()
export params

# https://gitlab.com/gitlab-org/gitlab-foss/-/issues/17861#note_19140733
if [[ $CI_USE_CACHE == true ]]; then
  if docker pull "$CI_BUILD_IMAGE_TAG"; then
    echo "Using existing image $CI_BUILD_IMAGE_TAG as cache source"
    params+=(--cache-from "$CI_BUILD_IMAGE_TAG")
  elif docker pull "$CI_BUILD_IMAGE_BASE:main"; then
    echo "Falling back to $CI_BUILD_IMAGE_BASE:main as cache source"
    params+=(--cache-from "$CI_BUILD_IMAGE_BASE:main")
  else
    echo "No available cache source found, building fresh"
    params+=(--no-cache)
  fi
else
  echo "CI_USE_CACHE is set to false"
  params+=(--no-cache)
fi

docker build "${params[@]}" --target build -t "${CI_BUILD_IMAGE_TAG}" .
docker build --cache-from "${CI_BUILD_IMAGE_TAG}" -t "${CI_IMAGE_TAG}" .
