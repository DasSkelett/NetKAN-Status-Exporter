//go:build go1.18
// +build go1.18

package main

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"
	"unicode/utf8"
)

func FuzzParseStatus(f *testing.F) {
	f.Add(false, true,
		"2019-11-20T05:32:13+00:00", "2019-11-20T05:32:13+00:00", "", "2019-11-20T05:32:13+00:00",
		"2019-11-20T05:32:13+00:00", "warning")

	f.Fuzz(func(t *testing.T, frozen, success bool, lc, ld, le, lid, lif, lw string) {
		if !utf8.ValidString(lc) ||
			!utf8.ValidString(ld) ||
			!utf8.ValidString(le) ||
			!utf8.ValidString(lid) ||
			!utf8.ValidString(lif) ||
			!utf8.ValidString(lw) {
			t.SkipNow()
		}

		module := Module{
			Frozen:         frozen,
			Success:        success,
			LastChecked:    lc,
			LastDownloaded: ld,
			LastError:      le,
			LastIndexed:    lid,
			LastInflated:   lif,
			LastWarnings:   lw,
		}
		status := Status{"0": module}
		statusJSON, err := json.Marshal(status)
		if err != nil {
			t.Error(err)
		}

		sd, err := parseStatus(bytes.NewReader(statusJSON), false)
		if err != nil {
			t.Fatal(err)
		}
		m := (*sd)["0"]
		if m.Frozen != module.Frozen ||
			m.Success != module.Success ||
			m.LastChecked != module.LastChecked ||
			m.LastDownloaded != module.LastDownloaded ||
			m.LastError != module.LastError ||
			m.LastIndexed != module.LastIndexed ||
			m.LastInflated != module.LastInflated ||
			m.LastWarnings != module.LastWarnings {
			t.Errorf("parsed module does not match original module: expected %v got %v", module, m)
		}
		if !reflect.DeepEqual(m, module) {
			t.Errorf("parsed module does not deeply equal original module: expected %v got %v", module, m)
		}
	})
}
