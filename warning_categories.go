package main

import "regexp"

var (
	vrefNoVersionFileRegex = regexp.MustCompile(
		"(^|\r\n)\\$vref present, version file missing")
	versionFileNoVrefRegex = regexp.MustCompile(
		"(^|\r\n)\\$vref absent, version file present")
	remoteVersionFileFetchErrorRegex = regexp.MustCompile(
		"(^|\r\n)Error fetching remote version file")
	remoteVersionFileParseErrorRegex = regexp.MustCompile(
		"(^|\r\n)Error parsing remote version file")
	mmDepNoMmSyntaxRegex = regexp.MustCompile(
		"(^|\r\n)ModuleManager dependency may not be needed, no ModuleManager syntax found")
	mmSyntaxNoMmDepRegex = regexp.MustCompile(
		"(^|\r\n)ModuleManager syntax used without ModuleManager dependency")
	unboundedCompatibilityRegex = regexp.MustCompile(
		"(^|\r\n)Unbounded future compatibility for module with a plugin")
	githubURLUnknownFormatRegex = regexp.MustCompile(
		"(^|\r\n)Remote non-raw GitHub URL is in an unknown format, using as is")
	dllsNotAutodetectedRegex = regexp.MustCompile(
		"(^|\r\n)No plugin matching the identifier, manual installations won't be detected")

	allCategories = []WarningCategory{
		vrefNoVersionFile{},
		versionFileNoVref{},
		remoteVersionFileFetchError{},
		remoteVersionFileParseError{},
		mmDepNoMmSyntax{},
		mmSyntaxNoMmDep{},
		unboundedCompatibility{},
		githubURLUnknownFormat{},
		dllsNotAutodetected{},
	}
)

// WarningCategory defines the interface for all warning types with a regex to detect it and a Prometheus label string.
type WarningCategory interface {
	regex() *regexp.Regexp
	label() string
}

// GetAllWarningCategories returns a slice of WarningCategory for all known categories.
func GetAllWarningCategories() []WarningCategory {
	return allCategories
}

func collectWarningCategories(
	message string, categories *[]WarningCategory, categoryCounts *[]AtomicCounter) (anyHit bool) {
	for i, cat := range *categories {
		if cat.regex().MatchString(message) {
			(*categoryCounts)[i].Inc()
			anyHit = true
		}
	}

	return anyHit
}

/*
	vref present, version file missing
*/

type vrefNoVersionFile struct{}

func (vrefNoVersionFile) regex() *regexp.Regexp { return vrefNoVersionFileRegex }
func (vrefNoVersionFile) label() string         { return "vref_no_versionFile" }

/*
	version file present, vref absent
*/

type versionFileNoVref struct{}

func (versionFileNoVref) regex() *regexp.Regexp { return versionFileNoVrefRegex }
func (versionFileNoVref) label() string         { return "versionFile_no_vref" }

/*
	Error fetching remote version file
*/

type remoteVersionFileFetchError struct{}

func (remoteVersionFileFetchError) regex() *regexp.Regexp { return remoteVersionFileFetchErrorRegex }
func (remoteVersionFileFetchError) label() string         { return "remoteVersionFile_fetch_error" }

/*
	Error parsing remote version file
*/

type remoteVersionFileParseError struct{}

func (remoteVersionFileParseError) regex() *regexp.Regexp { return remoteVersionFileParseErrorRegex }
func (remoteVersionFileParseError) label() string         { return "remoteVersionFile_parse_error" }

/*
	ModuleManager dependency may not be needed, no ModuleManager syntax found
*/

type mmDepNoMmSyntax struct{}

func (mmDepNoMmSyntax) regex() *regexp.Regexp { return mmDepNoMmSyntaxRegex }
func (mmDepNoMmSyntax) label() string         { return "mm_dependency_no_syntax" }

/*
	ModuleManager syntax used without ModuleManager dependency
*/

type mmSyntaxNoMmDep struct{}

func (mmSyntaxNoMmDep) regex() *regexp.Regexp { return mmSyntaxNoMmDepRegex }
func (mmSyntaxNoMmDep) label() string         { return "mm_syntax_no_dependency" }

/*
	Unbounded future compatibility for module with a plugin
*/

type unboundedCompatibility struct{}

func (unboundedCompatibility) regex() *regexp.Regexp { return unboundedCompatibilityRegex }
func (unboundedCompatibility) label() string         { return "unbounded_compatibility" }

/*
	Remote non-raw GitHub URL is in an unknown format, using as is
*/

type githubURLUnknownFormat struct{}

func (githubURLUnknownFormat) regex() *regexp.Regexp { return githubURLUnknownFormatRegex }
func (githubURLUnknownFormat) label() string         { return "github_url_unknown_format" }

/*
	No plugin matching identifier, mod can't be autodetected
*/

type dllsNotAutodetected struct{}

func (dllsNotAutodetected) regex() *regexp.Regexp { return dllsNotAutodetectedRegex }
func (dllsNotAutodetected) label() string         { return "dlls_not_autodetected" }
