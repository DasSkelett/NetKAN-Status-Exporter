package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"runtime"
	"sync"
	"sync/atomic"
	"time"

	goJSON "github.com/goccy/go-json"
	"github.com/prometheus/client_golang/prometheus"
)

// Module represents a single module as represented in the status JSON file.
type Module struct {
	Frozen         bool   `json:"frozen"`
	LastChecked    string `json:"last_checked"`
	LastDownloaded string `json:"last_downloaded"`
	LastError      string `json:"last_error"`
	LastIndexed    string `json:"last_indexed"`
	LastInflated   string `json:"last_inflated"`
	LastWarnings   string `json:"last_warnings"`
	// Resources struct {
	//	Repository string `json:"repository"`
	//	SpaceDock  string `json:"spacedock"`
	//	Homepage   string `json:"homepage"`
	//	Metanetkan string `json:"metanetkan"`
	//	RemoteAvc  string `json:"remote_avc"`
	// }
	Resources map[string]string `json:"resources"`
	Success   bool              `json:"success"`
}

// Status represents the overall format of the JSON file as returned by the http://status.ksp-ckan.space API.
type Status map[string]Module

type AtomicCounter struct {
	c uint32
}

func (c *AtomicCounter) Inc() {
	atomic.AddUint32(&c.c, 1)
}
func (c *AtomicCounter) AsFloat64() float64 {
	return float64(atomic.LoadUint32(&c.c))
}
func (c *AtomicCounter) Reset() {
	atomic.StoreUint32(&c.c, 0)
}

type modMetrics struct {
	activeMods, frozenMods, modsWithWarnings, modsWithErrors, successfulMods, failedMods,
	metanetkans, statusDumpTime, statusDownloadDuration, statusParsingDuration prometheus.Gauge
	modsByWarningCategory prometheus.GaugeVec
}

type modMetricsCounters struct {
	activeModsCount, frozenModsCount, modsWithWarningsCount, modsWithErrorsCount, successfulModsCount,
	failedModsCount, warningsOthers, metanetkansCount AtomicCounter
	warningCategories       *[]WarningCategory
	warningCategoriesCounts *[]AtomicCounter
}

type statusCache struct {
	maxAge              time.Duration
	lastStatus          *Status
	lastStatusTimestamp time.Time
}

type StatusExporter struct {
	Source   string
	cache    *statusCache
	metrics  modMetrics
	counters modMetricsCounters
}

func NewStatusExporter(statusSource string, cacheMaxAge time.Duration) *StatusExporter {
	warningCategories := GetAllWarningCategories()
	warningCategoriesCounts := make([]AtomicCounter, len(warningCategories))

	return &StatusExporter{
		Source: statusSource,
		metrics: modMetrics{
			activeMods: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: "netkan_mods_active",
				Help: "Number of mods (i.e. netkans) being actively monitored for updates",
			}),
			frozenMods: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: "netkan_mods_frozen",
				Help: "Number of mods (i.e. netkans) that are frozen and not actively monitored for updates",
			}),
			modsWithWarnings: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: "netkan_mods_with_warnings",
				Help: "Number of mods (i.e. netkans) with one or more warnings",
			}),
			modsWithErrors: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: "netkan_mods_with_error",
				Help: "Number of mods (i.e. netkans) with an inflation error",
			}),
			failedMods: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: "netkan_mods_failed",
				Help: "Number of mods (i.e. netkans) that failed inflation",
			}),
			successfulMods: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: "netkan_mods_successful",
				Help: "Number of mods (i.e. netkans) that succeeded inflation",
			}),
			metanetkans: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: "netkan_metanetkans",
				Help: "Number of active metanetkans",
			}),
			statusDumpTime: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: "netkan_status_dump_time",
				Help: "Time of the last status dump from database to the JSON file",
			}),
			statusDownloadDuration: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: "netkan_status_download_duration_milliseconds",
				Help: "The time it took to download the status JSON in milliseconds",
			}),
			statusParsingDuration: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: "netkan_status_parsing_duration_milliseconds",
				Help: "The time it took to parse and load the status JSON in milliseconds",
			}),
			modsByWarningCategory: *prometheus.NewGaugeVec(prometheus.GaugeOpts{
				Name: "netkan_mods_by_warning_category",
				Help: "Number of mods (i.e. netkans) labelled by warning category (mods may appear more than once)",
			},
				[]string{"warning_category"},
			),
		},

		counters: modMetricsCounters{
			warningCategories:       &warningCategories,
			warningCategoriesCounts: &warningCategoriesCounts,
		},

		cache: &statusCache{
			maxAge: cacheMaxAge,
		},
	}
}

func (e StatusExporter) Describe(descs chan<- *prometheus.Desc) {
	descs <- e.metrics.activeMods.Desc()
	descs <- e.metrics.frozenMods.Desc()
	e.metrics.modsByWarningCategory.Describe(descs)
	descs <- e.metrics.modsWithWarnings.Desc()
	descs <- e.metrics.modsWithErrors.Desc()
	descs <- e.metrics.failedMods.Desc()
	descs <- e.metrics.successfulMods.Desc()
	descs <- e.metrics.statusDumpTime.Desc()
	descs <- e.metrics.statusDownloadDuration.Desc()
	descs <- e.metrics.statusParsingDuration.Desc()
	descs <- e.metrics.metanetkans.Desc()
}

func (e StatusExporter) Collect(metrics chan<- prometheus.Metric) {
	e.collect(true)

	metrics <- e.metrics.activeMods
	metrics <- e.metrics.frozenMods
	e.metrics.modsByWarningCategory.Collect(metrics)
	metrics <- e.metrics.modsWithWarnings
	metrics <- e.metrics.modsWithErrors
	metrics <- e.metrics.failedMods
	metrics <- e.metrics.successfulMods
	metrics <- e.metrics.statusDumpTime
	metrics <- e.metrics.statusDownloadDuration
	metrics <- e.metrics.statusParsingDuration
	metrics <- e.metrics.metanetkans

	// The Status object can be quite big (>270MB), let's give the GC a hint.
	go func() {
		time.Sleep(5 * time.Second)
		runtime.GC()
	}()
}

func resetMetricCounters(mc *modMetricsCounters) {
	mc.activeModsCount.Reset()
	mc.frozenModsCount.Reset()
	mc.modsWithWarningsCount.Reset()
	mc.modsWithErrorsCount.Reset()
	mc.successfulModsCount.Reset()
	mc.failedModsCount.Reset()
	mc.warningsOthers.Reset()
	mc.metanetkansCount.Reset()
	for i := range *mc.warningCategoriesCounts {
		(*mc.warningCategoriesCounts)[i].Reset()
	}
}

func (e *StatusExporter) collect(concurrent bool) {
	statusData, stats, err := fetchStatus(e.Source, e.cache)
	if err != nil {
		log.Fatal(err)
	}

	resetMetricCounters(&e.counters)
	e.metrics.statusDownloadDuration.Set(float64(stats.downloadDuration.Milliseconds()))
	e.metrics.statusParsingDuration.Set(float64(stats.parsingDuration.Milliseconds()))

	e.processMods(concurrent, 1024, statusData)
}

func (e *StatusExporter) processMods(concurrent bool, chanBufferSize int, statusData *Status) {
	// Spawn a group of workers to process the mods concurrently
	var maxWorkers int
	if concurrent {
		maxWorkers = runtime.NumCPU()
	} else {
		maxWorkers = 1
	}

	inputData := make(chan Module, chanBufferSize)
	wg := sync.WaitGroup{}

	for i := 1; i <= maxWorkers; i++ {
		wg.Add(1)
		go collectWorker(&e.counters, inputData, &wg)
	}
	for _, mod := range *statusData {
		inputData <- mod
	}
	close(inputData)
	// Wait until all workers are done
	wg.Wait()

	e.metrics.activeMods.Set(e.counters.activeModsCount.AsFloat64())
	e.metrics.frozenMods.Set(e.counters.frozenModsCount.AsFloat64())
	for i, cat := range *e.counters.warningCategories {
		e.metrics.modsByWarningCategory.With(prometheus.Labels{"warning_category": cat.label()}).
			Set((*e.counters.warningCategoriesCounts)[i].AsFloat64())
	}
	e.metrics.modsByWarningCategory.With(prometheus.Labels{"warning_category": "others"}).
		Set(e.counters.warningsOthers.AsFloat64())
	e.metrics.modsWithWarnings.Set(e.counters.modsWithWarningsCount.AsFloat64())
	e.metrics.modsWithErrors.Set(e.counters.modsWithErrorsCount.AsFloat64())
	e.metrics.failedMods.Set(e.counters.failedModsCount.AsFloat64())
	e.metrics.successfulMods.Set(e.counters.successfulModsCount.AsFloat64())
	e.metrics.metanetkans.Set(e.counters.metanetkansCount.AsFloat64())
	e.metrics.statusDumpTime.Set(float64(e.cache.lastStatusTimestamp.UnixNano()))
}

func collectWorker(metrics *modMetricsCounters, inputData <-chan Module, wg *sync.WaitGroup) {
	defer wg.Done()
	for mod := range inputData {
		if mod.Frozen {
			metrics.frozenModsCount.Inc()
		} else {
			metrics.activeModsCount.Inc()

			// We don't care about warnings and errors of already frozen mods
			if mod.LastWarnings != "" {
				metrics.modsWithWarningsCount.Inc()

				others := !collectWarningCategories(mod.LastWarnings, metrics.warningCategories, metrics.warningCategoriesCounts)

				if others {
					metrics.warningsOthers.Inc()
				}
			}
			if mod.LastError != "" {
				metrics.modsWithErrorsCount.Inc()
			}
			if mod.Success {
				metrics.successfulModsCount.Inc()
			} else {
				metrics.failedModsCount.Inc()
			}
			if _, ok := mod.Resources["metanetkan"]; ok {
				metrics.metanetkansCount.Inc()
			}
		}
	}
}

type fetchStatistics struct {
	downloadDuration, parsingDuration time.Duration
}

// fetchStatus GETs the status JSON from the specified URL, or uses a cached one if it hasn't reached maxAge yet.
// fetchStatus returns a pointer to a parsed Status object,
// a fetchStatistics including with download and parsing duration, or an error.
func fetchStatus(url string, cache *statusCache) (*Status, fetchStatistics, error) {
	if cache == nil {
		return nil, fetchStatistics{}, errors.New("cache cannot be nil to be able to save the timestamp")
	}

	if cache.lastStatus != nil && time.Since(cache.lastStatusTimestamp) < cache.maxAge {
		fmt.Printf("Last status refresh less than %s ago, reusing previous status data\n", cache.maxAge.String())
		return cache.lastStatus, fetchStatistics{}, nil
	}
	if *debug {
		fmt.Println(time.Now().Truncate(time.Minute).Format("[15:04]") + " Downloading new status file")
	}

	// Get the data
	start := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		return nil, fetchStatistics{}, err
	}
	defer resp.Body.Close()
	downloadDuration := time.Since(start)
	cache.lastStatusTimestamp, err = time.Parse(time.RFC1123, resp.Header.Get("Last-Modified"))
	if err != nil {
		cache.lastStatusTimestamp = time.Now()
	}
	if *debug {
		fmt.Println("Download took " + downloadDuration.String())
		fmt.Println("Last-Modified " + resp.Header.Get("Last-Modified"))
	}
	start = time.Now()
	statusData, err := parseStatus(resp.Body, *useGoJSON)
	if err != nil {
		return nil, fetchStatistics{}, err
	}
	parsingDuration := time.Since(start)
	if *debug {
		fmt.Println("Parsing took " + parsingDuration.String())
	}
	if cache.maxAge > 0 {
		cache.lastStatus = statusData
	}

	return statusData, fetchStatistics{downloadDuration, parsingDuration}, nil
}

func parseStatus(reader io.Reader, useGoJSON bool) (statusData *Status, err error) {
	statusData = new(Status)
	if useGoJSON {
		err = goJSON.NewDecoder(reader).Decode(statusData)
	} else {
		err = json.NewDecoder(reader).Decode(statusData)
	}
	if err != nil {
		return nil, err
	}

	return statusData, nil
}
