FROM golang:1.22-alpine3.19 as build

EXPOSE 9811
WORKDIR /netkan-exporter
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . /netkan-exporter
RUN go test -vet=off .
RUN go build -o /go/bin/netkan-status-exporter
CMD [ "netkan-status-exporter" ]

FROM alpine:3.19
COPY --from=build /go/bin/netkan-status-exporter /usr/local/bin/
CMD [ "netkan-status-exporter" ]
