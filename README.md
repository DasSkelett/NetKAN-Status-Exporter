# NetKAN Status Exporter

A [Prometheus](https://prometheus.io/) exporter for the NetKAN infrastructure [status](http://status.ksp-ckan.space/) of the [KSP-CKAN](https://github.com/KSP-CKAN/CKAN) project.

License: GPLv3

## Usage
The exporter listens on `:9811` by default, it can be overwritten using the `--web.listen-address` commandline option, or `NETKAN_STATUS_EXPORTER_WEB_LISTEN_ADDRESS` environment variable.
Add a Prometheus scrape config to scrape it like this:
```yaml
scrape_configs:
  - job_name: 'netkan-status'
    scrape_interval: 30m
    scrape_timeout:  20s
    static_configs:
      - targets: [ 'exporter.host:9811' ]
```
The NetKAN inflator basically only runs every 30 minutes, so it makes sense to set the scrape_interval just as low to reduce load on the CKAN infrastructure.
However, Prometheus considers metrics not updated for more than 5 minutes [stale](https://prometheus.io/docs/prometheus/latest/querying/basics/#staleness) and won't return any value for them,
you can work around this by using `max_over_time(netkan_some_metric[30m])`.
You need a relatively high timeout (e.g. `20s`), since downloading and parsing the JSON can take quite a long time.

## Docker
There's a Docker image available at [registry.gitlab.com/dasskelett/netkan-status-exporter](https://gitlab.com/DasSkelett/NetKAN-Status-Exporter/container_registry/1804288) and [dasskelett/netkan-status-exporter](https://hub.docker.com/r/dasskelett/netkan-status-exporter).
It's built using `golang:alpine`, and the final image is based on `alpine:3.13` and thus only a few MiB in size.

## Third-party packages
This project uses
* [Prometheus Go client library](https://github.com/prometheus/client_golang) ([Guide](https://prometheus.io/docs/guides/go-application/)), Apache-2.0
* [Kingpin command line and flag parser](https://github.com/alecthomas/kingpin), MIT
* [goccy/go-json](https://github.com/goccy/go-json), MIT
  for optional faster JSON decoding. Warning: Experimental, has memory leaks
